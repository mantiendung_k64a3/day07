<?php
date_default_timezone_set("Asia/Ho_Chi_Minh");
$gender = array("2" => "Nam", "1" => "Nữ");
$arr = array(""=>"","MAT"=>"Khoa học máy tính","KDL" => "Khoa học vật liệu");
$name = '';
$khoa = '';
?>
<html>
    <head>
        <title>Document</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    </head>

<style type="text/css">
   
    .info{
        width: 100px;
        display: inline-block;
    }
    .profile{
        width: 50%px;
        padding: 5px 5px;
        margin-top: 5px;
        border-radius: 10px/20px;
    }
    .btn{
        display: inline-block;
        background-color: #4F81BD;
        padding: 5px 10px 5px 10px;
        border-radius: 10px/20px;
        cursor: pointer;
    }
    .count-student{
        margin-left: 10px;
    }

    .wrapper {	
        position: absolute;
        top: 20%;
        left: 35%;
        background: #eee !important;	
        border: 1px solid #000;
        padding: 5px 5px 5px 5px;
        border-radius: 10px/20px;
    }

    .no{
        width: 20;
    }
    .name{
        max-width: 300px;
        min-width: 150px;
    }
    .khoa{
        max-width: 300px;
        min-width: 150px;
    }
    .btn-add{
        float:right;
        margin-right: 20px;
        width: 70px;
    }
    .btn-delete{
        float:right;
        margin-right: 20px;
        width: 70px;
    }

    .form-signin{
        display: inline-block;
    }
</style>
<?php
    session_start();
    $name = isset($_POST['fullname']) ? $_POST['fullname'] : '';
    $khoa = isset($_POST['type']) ? $_POST['type'] : '';
?>
<body> 
    <div class = 'wrapper'>
        <form action="index.php" method="post">
            <div id= "khoa">
                <label class = 'info'>Phân khoa</label>
                
                <select style="padding: 5px 5px;border-radius: 10px/20px;" name="type" id="item_type">

                    <?php foreach ($arr as $key => $value) { ?>

                        <option class= 'profile' value="<?= $key ?>" <?php if ($key == $khoa) { echo 'selected="selected"';}?> > <?=$value?></option>
                        <!-- <option class= 'profile' <? if ($key == $khoa) {echo ('selected');}?> value="<?= $key ?>"><?=$value?></option> -->
                    <?php }?>
                </select>
            </div>
            <div>
                <label class = 'info'>Từ khoá</label>
                <input class = 'profile' id = "fullname" name="fullname" type='text' value=<?php echo $name?>>
            </div>
            <input class='btn btn-register' name='register' id = "search" type='submit' value='Tìm kiếm'>
        </form>
        <div>
            <br>
            <label class = 'count-student'>Số sinh viên tìm thấy: xxx</label>
            <br>
            <button class = 'btn btn-delete' id = "delete">Xoá</button>
            <button class = 'btn btn-add' id = "add">Thêm</button>
            <br>
            <table>
                <tr>
                    <th class="no">No</th>
                    <th class="name">Tên sinh viên</th>
                    <th class="khoa">Khoa</th>
                    <th class="action">Action</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Nguyễn Văn A</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <button class="btn">Xoá</button>
                        <button class="btn">Sửa</button>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Trần Thị B</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <button class="btn">Xoá</button>
                        <button class="btn">Sửa</button>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Nguyễn Hoàng C</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <button class="btn">Xoá</button>
                        <button class="btn">Sửa</button>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Đinh Quang D</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <button class="btn">Xoá</button>
                        <button class="btn">Sửa</button>
                    </td>
                </tr>
                <?php
                if(isset($_SESSION['fullname']) && !empty($_SESSION['fullname'])) {
                    echo '<tr>
                            <td>5</td>
                            <td>' . 
                            $_SESSION['fullname']
                            . '</td>
                        <td>' . $arr[$_SESSION['type']] .
                        '</td>
                        <td>
                        <button class="btn">Xoá</button>
                        <button class="btn" >Sửa</button>
                    </td>';
                }
                ?>
            </table>
        </div>
    </div>
    <script>
        $('#delete').click(function() {
            $('#fullname').val('');
            $("#khoa select").val('');s
        });
        $("#search").click(function(){

        });
    </script>
</body>
</html>