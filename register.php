<?php
date_default_timezone_set("Asia/Ho_Chi_Minh");
$gender = array("2" => "Nam", "1" => "Nữ");
$arr = array(""=>"","MAT"=>"Khoa học máy tính","KDL" => "Khoa học vật liệu");
?>
<html>
    <head>
        <link rel='stylesheet' href='//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>
        <title>Document</title>
    </head>

<style>
    body {
        background: #eee !important;	
    }
    .wrapper {	
        margin-top: 80px;
        margin-bottom: 80px;
    }
    .form-signin {
        max-width: 500px;
        padding: 15px 35px 45px;
        margin: 0 auto;
        background-color: #fff;
        border: 1px solid rgba(0,0,0,0.1);
        border-radius: 10px/20px;
        border: 1px solid #000;
    }
    
    .info{
        width: 30%;
        background: #70AD47;
        padding: 5px 5px;
        border: 1px solid #000;
        color: #fff;
        text-align: center;
    }
    .profile{
        width: 50%px;
        padding: 5px 5px;
        margin-top: 5px;
        border-radius: 10px/20px;
    }
    .btn{
        margin-top:10px;
        max-width: 180px;
        margin-right: auto;
        margin-left: auto;
        background: #70AD47;;
    }

    .btn:hover{
        background-color: green;
    }

    .container{
        display: inline-block;
        position:relative;
        cursor:pointer;
        font-size:18px;
        user-select: none;
        padding-left:30px;
        padding-right:40px;
        margin-bottom:10px;
    }
    .container input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }
    .checkmark{
        position:absolute;
        top:0;
        left:0;
        width:20px;
        height:20px;
        background:#eee;
        border-radius:50%;
    }
    .container:hover .checkmark{
        background:#ccc;
    }
    .checkmark:after{
        content:"";
        position:absolute;
        display:none;
    }
    .container .checkmark:after{
        top:50%;
        left:50%;
        width: 15px;
        height: 15px;
        border-radius:50%;
        border: solid 3px white;
        transform:translate(-50%,-50%) rotate(45deg);
    }
    .container input:checked ~ .checkmark{
        background:#2196F3;
    }
    .container input:checked ~ .checkmark:after{
        display:block;
    }
</style>
   
<body>
    <?php
        
        $error = array();
        session_start();
        
        if (!empty($_POST['register'])){
            $_SESSION['fullname'] = isset($_POST['fullname']) ? $_POST['fullname'] : '';
            $_SESSION['gender'] = isset($_POST['gender']) ? $_POST['gender'] : '';
            $_SESSION['type'] = isset($_POST['type']) ? $_POST['type'] : ''; 
            $_SESSION['date'] = isset($_POST['date']) ? $_POST['date'] : '';
            $_SESSION['address'] = isset($_POST['address']) ? $_POST['address'] : '';
            function validateDateFormat($date,$format='d/m/Y'){
                $dt = DateTime::createFromFormat($format, $date);
                return $dt !== false && !array_sum($dt->getLastErrors());
            }
            function isImage($image){
                $extension = pathinfo($image, PATHINFO_EXTENSION);
                $imgExtArr = ['jpg', 'jpeg', 'png'];
                if(in_array($extension, $imgExtArr)){
                    return true;
                }
                return false;
            }
            if (empty($_SESSION['fullname'])){
                $error['fullname'] = 'Hãy nhập tên.';
                
            }
            if (empty($_SESSION['gender'])){
                $error['gender'] = 'Hãy chọn giới tính.';
            }
            if (empty($_SESSION['type'])){
                $error['type'] = 'Hãy chọn phân khoa.';
            }
            if (empty($_SESSION['date'])){
                $error['date'] = 'Hãy nhập ngày sinh';
            }
            if(!validateDateFormat($_SESSION['date'])) {
                $error['date'] = 'Hãy nhập ngày sinh đúng định dạng';
            }
            if(!(strlen(strstr($_FILES['image']['type'], 'image')) > 0)){
                $error['file'] = 'Không đúng định dạng';
            }
            $a = $_FILES['image']['name'];
            $ext = pathinfo($a);
            $maxfilesize   = 41943040;
            if ($_FILES["image"]["size"] > $maxfilesize)
            {
                $error['file'] = "Không được upload ảnh lớn hơn $maxfilesize (bytes).";
            }
            if(empty($error)){
                if(!file_exists('uploads')){
                    mkdir("uploads",0700);
                }
                $_SESSION['image'] = "uploads/" . $ext['filename'] . date("YmdHis") . "." . $ext['extension'];
                $image = $_SESSION['image'];
                $image_tmp_name= $_FILES['image']['tmp_name'];
                move_uploaded_file($image_tmp_name,$_SESSION['image']);
                header("Location: http://localhost/day07/index.php", true, 301);
            }
        }

    ?>
<div class = 'wrapper'>
<form class = 'form-signin' action='register.php' method='post' enctype="multipart/form-data">
    <div style = "color: red;">
        <?php foreach ($error as $e){?>
            <?php echo $e; ?>
            <br>
        <?php } ?>
    </div>
    
    <div>
        <label class = 'info'>Họ và tên<span style="color: red;">*</span></label>
        <input class = 'profile' name="fullname" type='text' placeholder='Tên đăng nhập'>
    </div>

    <div>
        <label class = 'info'>Giới tính<span style="color: red;">*</span></label>
        <?php foreach ($gender as $key => $value) { ?>
            <label style="width: 50px;" class="container">
                <input type='radio' id='gender' name='gender' value= <?=$key?>>
                <label for='gender'><?= $value?></label>
                <span class="checkmark"></span>
            </label>
        <?php } ?>  
    </div>

    <div>
        <label class = 'info'>Phân khoa<span style="color: red;">*</span></label>
        
        <select style="padding: 5px 5px;border-radius: 10px/20px;" name="type" id="item_type">
            <?php foreach ($arr as $key => $value) { ?>
                    <option class= 'profile' value="<?= $key ?>"><?=$value?></option>
            <?php } ?>
        </select>
    </div>

    <div>
        <label class = 'info'>Ngày sinh<span style="color: red;">*</span></label>
        <input class = 'profile' name="date" type='text' placeholder='dd/mm/yyyy'>
    </div>
    <div>
        <label class = 'info'>Địa chỉ</label>
        <input class = 'profile' name="address" type='text'>
    </div>

    <div>
        <label class = 'info'>Hình ảnh</label>
        <input style="display: inline-block;" class = 'profile' name="image" id="image" type='file'>
    </div>
    <input class='btn btn-lg btn-primary btn-block' name='register' type='submit' value='Đăng ký'>
</form>
</div>
</body>
</html>